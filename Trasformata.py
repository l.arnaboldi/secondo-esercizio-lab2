import pylab
import numpy

#Definisco la funzione che restituisca l'array delle frequenze
def trasformata_x(array_in):
    xx = numpy.linspace(0, len(array_in)/(2 * array_in[len(array_in) - 1]), 1 + len(array_in) / 2)
    return xx


#Includo il file

t, V = pylab.loadtxt('esercitazioni/11/0_57.txt', unpack = 'True')


t /= 1000000.

#Ne faccio la trasformata

xx = trasformata_x(t)

yy = numpy.log(abs(numpy.fft.rfft(V)))

#Grafico entrambi

pylab.subplot(2, 1, 1)

pylab.plot(t, V, color = 'red')

#Bellurie primo grafico
#pylab.rc('font',size=20)
pylab.xlabel('$t$ [s]')
pylab.ylabel('$V(t)$ [digit]')
pylab.minorticks_on()

pylab.subplot(2, 1, 2)

pylab.plot(xx, yy, color = 'green')

#Bellurie secondo grafico
#pylab.rc('font',size=8)
pylab.xlabel('$f$ [Hz]')
pylab.ylabel('$V(t)$ [arb. un.]')
pylab.minorticks_on()

pylab.show()
