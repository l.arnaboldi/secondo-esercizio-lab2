import pylab
import numpy
from scipy.optimize import curve_fit

# data load
x,y=pylab.loadtxt('long.txt',unpack=True)
Dx = numpy.array([0.]*len(x))
Dy = numpy.array([0.]*len(y))
# scatter plot with error bars
pylab.errorbar(x,y,Dy,Dx,linestyle = '-', color = 'black', marker = '.')
pylab.show()
