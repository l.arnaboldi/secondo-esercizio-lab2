
#NON TI PERMETTERE di usarlo
import pylab
import numpy
from scipy.optimize import curve_fit

"""
# data load
x,Dx,y,Dy=pylab.loadtxt('nomefile.txt',unpack=True)
"""

# define the function (linear, in this example)
def ff(x, V0, tau, w, phi, off):
    return V0*numpy.exp(-x/tau)*numpy.cos(w*x + phi) + off
    
init=(400, 15., 1.0, numpy.pi/2, 450)
  
x,y=pylab.loadtxt('ferropieno.txt',unpack=True)
Dx = numpy.array([4.]*len(x))
x /= 1e3
Dx /= 1e3

Dy = numpy.array([1.]*len(y))

# use subplots to display two plots in one figure
# note the syntax


#Butta outliers
sigma=Dy
w=1/sigma**2
pars,covm=curve_fit(ff,x,y,init,sigma)
rq = w*(y-ff(x,*pars))**2
outliers_indices = []
for i in range(0, len(rq)):
  if numpy.sqrt(rq[i]) > 20.:
    outliers_indices.append(i)

outx = []
outDx = []
outy = []
outDy = []
outx = numpy.take(x, outliers_indices)
x = numpy.delete(x, outliers_indices)

outy = numpy.take(y, outliers_indices)
y = numpy.delete(y, outliers_indices)

outDx = numpy.take(Dx, outliers_indices)
Dx = numpy.delete(Dx, outliers_indices)

outDy = numpy.take(Dy, outliers_indices)
Dy = numpy.delete(Dy, outliers_indices)
    


pylab.subplot(2,1,2)

# scatter plot with error bars
pylab.errorbar(outx,outy,outDy,outDx,linestyle = '', color = 'orange', marker = '.')
pylab.errorbar(x,y,Dy,Dx,linestyle = '', color = 'black', marker = '.')


# bellurie 
pylab.rc('font',size=18)
pylab.xlabel('$t$  [ms]')
pylab.ylabel('$V$  [arb. un.]')
pylab.minorticks_on()

# AT THE FIRST ATTEMPT COMMENT FROM HERE TO THE END    

# define the initial values (STRICTLY NEEDED!!!)

# prepare a dummy xx array (with 2000 linearly spaced points)
xx=numpy.linspace(min(x), max(x),2000)

# plot the fitting curve computed with initial values
# AT THE SECOND ATTEMPT THE FOLLOWING LINE MUST BE COMMENTED 
#pylab.plot(xx,ff(xx,*init), color='blue') 

# set the error
sigma=Dy
w=1/sigma**2

# call the minimization routine
pars,covm=curve_fit(ff,x,y,init,sigma)

# calculate the chisquare for the best-fit function
chi2 = ((w*(y-ff(x,*pars))**2)).sum()

# determine the ndof
ndof=len(x)-len(init)

# print results on the console
print('pars:',pars)
print('covm:',covm)
print ('chi2, ndof:',chi2, ndof)

# plot the best fit curve
pylab.plot(xx,ff(xx,*pars), color='red') 

# switch to the residual plot
pylab.subplot(2,1,1)

# build the array of the normalized residuals
r = (y-ff(x,*pars))/sigma

# bellurie 
pylab.rc('font',size=18)
pylab.ylabel('Norm. res.')
pylab.minorticks_on()
# set the vertical range for the norm res
#pylab.ylim((-.9,.9))

# plot residuals as a scatter plot with connecting dashed lines
pylab.plot(x,r,linestyle="--",color='blue',marker='o')
"""
L = pars[2]**2 / (4*numpy.pi**2 * C)
r = 2000 * L / pars[1]

print(L, r)

"""
print([numpy.sqrt(covm[i][i]) for i in range(0,5)]) 

# show the plot
pylab.show()