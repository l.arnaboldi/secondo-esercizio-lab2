
#  transform.py
#  Questo file legge il contenuto del generation_file e crea il corrispetttivo 
#  file TeX con tutte le immagini necessarie.
#  

import numpy
import matplotlib.pyplot as plot

pdfdir = 'immagini/'
generation_file = 'toplot.txt'
tex_file = 'contenuto.tex'

def trasform(t, V_t, figfile = None, show = False, logscale = False):
    
    f = numpy.linspace(0, len(t)/(2 * t[-1]), int((len(t) + 2) / 2))
    V_f = abs(numpy.fft.rfft(V_t))
    
    fig = plot.figure()
    fig.set_size_inches(7.2, 4.5)
    fig.subplots_adjust(hspace = 8.0)
    data = plot.subplot2grid((12,10), (0,0), colspan = 9, rowspan = 5, fig = fig)
    trasformed = plot.subplot2grid((12,10), (6,0), colspan = 9, rowspan = 5, fig = fig)
    data.plot(t, V_t, color = 'red', linewidth = 0.5)
    data.set_xlabel('$t$ [s]')
    data.set_ylabel('$V(t)$ [digit]')
    data.minorticks_on()
    
    trasformed.plot(f, V_f, color = 'blue', linewidth = 0.5)
    if logscale:
        trasformed.set_xscale('log')
    trasformed.set_yscale('log')
    trasformed.set_xlabel('$f$ [Hz]')
    trasformed.set_ylabel('$\widetilde{V}(f)$ [a.u.]')
    trasformed.minorticks_on()
    
    if show:
        plot.show()
        
    if figfile is not None:
        fig.savefig(pdfdir + figfile + '.pdf', format = 'pdf', bbox_inches = 'tight')
    plot.close(fig)


def main(args):
    tex = "% File generato automaticamente da trasform.py\n\n"
    with open(generation_file, 'r') as gfile:
        for line in gfile:
            line = line[:-1] # Remove last char (it is always an endline)
            if len(line) is 0 or line[0] is '#':
                continue
                
            print(line.split('\t'))
            try:
                path, figname, caption, scale = line.split('\t')
                t, V_t = numpy.loadtxt(path, unpack = True)
                t /= 1000000. # Convptpython3ert us to s
                trasform(t, V_t, figname + 'normal')
                if scale == 'l' or scale == 'b':
                    trasform(t, V_t, figname + 'log', logscale = True)
                # Generation of TeX code for the image
                tex += "\\begin{figure}[!htb]\n"
                tex += "    \\centering\n"
                if scale == 'n' or scale == 'b':
                    tex += "    \\includegraphics[scale = 1.0]{{immagini/{0}}}\n".format(figname + 'normal' + '.pdf')
                if scale == 'l':
                    tex += "    \\includegraphics[scale = 1.0]{{immagini/{0}}}\n".format(figname + 'log' + '.pdf')
                if scale == 'b':
                    tex += "    \\includegraphics[scale = 1.0, trim={{0 0 0 5.0cm}}, clip]{{immagini/{0}}}\n".format(figname + 'log' + '.pdf')
                if caption is not 'nocap':
                    tex += "    \\caption{{{0}}}\n".format(caption)  
                    tex += "    \\label{{fig:{0}}}\n".format(figname)            
                tex += "\\end{figure}\n\n"
            except:
                typ, contenet = line.split('\t')
                if typ == 'sec':
                    tex += "\\section{{{0}}}\n".format(contenet)
                elif typ == 'sub':
                    tex += "\\subsection{{{0}}}\n".format(contenet)
                elif typ == 'text':
                    tex += contenet + '\n'
                elif typ == 'tex':
                    tex += "\\input{{{0}}}\n".format(contenet)
                else:
                    print("Dunno what to do with this line! :(")
                
    # Save TeX file
    with open(tex_file, 'w') as tfile:
        tfile.write(tex)
        
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
